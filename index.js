

let registeredUsers = [
  "James Jeffries",
  "Gunther Smith",
  "Macie West",
  "Michelle Queen",
  "Shane Miguelito",
  "Fernando Dela Cruz",
  "Akiko Yukihime"
];


function registerUser(username) {
  const userIndex = registeredUsers.indexOf(username);
  if (userIndex !== -1) {
    alert("Registration failed. Username already exists!");
  } else {
    registeredUsers.push(username);
    alert("Thank you for registering!");
  }
}

registerUser("Conan O. Brien");
console.log(registeredUsers);



let friendsList = [];
function addFriend(username) {
  const userIndex = registeredUsers.indexOf(username);
  if (userIndex !== -1) {
    friendsList.push(username);
    alert(`You have added ${username} as a friend!`);
  } else {
    alert("User not found.");
  }
}

addFriend();
console.log(friendsList);


function displayFriendsList() {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    friendsList.forEach(friend => console.log(friend));
  }
}
console.log(friendsList.length);

displayFriendsList();



function displayNumberFriends() {
  if (friendsList.length === 0) {
    console.log("You currently have 0 friends. Add one first.");
  } else{
    console.log(`You currently have ${friendsList.length} friends.`);
  }
}

displayNumberFriends();

function deleteLastFriend() {
  if (friendsList.length === 0) {
    alert("You currently have 0 friends. Add one first.");
  } else {
    const lastFriend = friendsList.pop();
    alert(`You have deleted ${lastFriend} from your friendsList.`);
  }
}

deleteLastFriend();
console.log(friendsList);


function deleteFriend(username) {
  const userIndex = friendsList.indexOf(username);
  if (userIndex === -1) {
    alert("User not found.");
  } else {
    friendsList.splice(userIndex, 1);
    alert(`You have deleted ${username} from your friendsList.`);
  }
}


deleteFriend();
console.log(friendsList);
